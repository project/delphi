INTRODUCTION
------------

The Delphi module provides an implementation of the
[Delphi](https://en.wikipedia.org/wiki/Delphi_method) method for
http://armstrong.wharton.upenn.edu. With some work it could be useful to other sites.


REQUIREMENTS
------------

* Node clone (https://www.drupal.org/project/node_clone)
* Entity reference (https://www.drupal.org/project/entityreference)
* Invite (https://www.drupal.org/project/invite)
* Token (https://www.drupal.org/project/token)
* Views Bulk Operations (VBO)
  (https://www.drupal.org/project/views_bulk_operations)
* Webform (https://www.drupal.org/project/webform)


INSTALLATION
------------

Install as you would normally install a contributed module.


KNOWN ISSUES
------------

This module was built for a specific site, so certain assumptions are made that
may not be generally useful. Also, this module depends on content types,
fields, and other configurations that are not setup in delphi.install


MAINTAINERS
-----------

Current maintainers:
 * Jacob Embree (jacobembree) - https://www.drupal.org/u/jacobembree
