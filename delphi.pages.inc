<?php

/**
 * @file
 * Functions needed for non-administration pages.
 */

/**
 * Page callback for redirecting to the current round.
 *
 * @param $session
 *   The session node.
 */
function delphi_session_current_round_page($session) {
  $session_wrapper = entity_metadata_wrapper('node', $session);
  drupal_goto('node/' . $session_wrapper->current_round_node->getIdentifier() . '/round');
}

/**
 * Page callback for creating the next round.
 *
 * @param $session
 *   The node object of the type "session" that needs a new round.
 */
function delphi_create_round_page($session) {
  $wrapper = entity_metadata_wrapper('node', $session);
  if (!$wrapper->current_round_node->getIdentifier()) {
    drupal_set_message(t("Error loading the session's current round.  Please contact the administrator"), 'error');
    watchdog('delphi', "Error loading %name session's current round for starting the next.", array(
      "%name" => $session->title,
    ), WATCHDOG_ERROR);
    drupal_goto('node/' . $session->nid);
  }
  if (lock_acquire("delphi_batch_round_" . $wrapper->current_round_node->getIdentifier())) {
    $batch = array(
      'title' => 'Creating the next round',
      'init_message' => t('Setting up'),
      'progress_message' => t('Working on @current of @total.'),
      'finished' => 'delphi_release_lock',
      'file' => drupal_get_path('module', 'delphi') . '/delphi.pages.inc',
      'operations' => array(
        array('delphi_generate_round_operation', array($session)),
        array('delphi_clone_questions_operation', array(
          delphi_round_load_questions($wrapper->current_round_node->value()),
        )),
        array('delphi_clone_invites_operation', array(
          delphi_round_load_invites($wrapper->current_round_node->value()),
        )),
      ),
    );
    batch_set($batch);
    batch_process('node/' . $session->nid . '/session/round');
  }
  else {
    drupal_set_message(t("Your other operation is not yet complete.  Please wait for it to finish then try again."), 'error');
    drupal_goto();
  }
}

/**
 * Batch API callback to generate a round node for a session.
 *
 * @param object $session
 *   The session that needs a new round.
 */
function delphi_generate_round_operation($session, &$context) {
  $wrapper = entity_metadata_wrapper('node', $session);
  $round = delphi_round_node_insert($session, $wrapper->field_current_round->value() + 1);
  $wrapper->field_current_round = $wrapper->field_current_round->value() + 1;
  $wrapper->save();
}

/**
 * Batch API callback to clone questions from a round.
 *
 * @param array $nids
 *   The node ID's of the questions to clone.
 */
function delphi_clone_questions_operation($nids, &$context) {
  module_load_include('inc', 'clone', 'clone.pages');
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current'] = 0;
  }
  $chunk_size = 5; // Arbitrary.
  foreach (array_slice($nids, $context['sandbox']['progress']) as $nid) {
    clone_node_save($nid);
    $node = node_load($nid);
    $context['sandbox']['progress']++;
    $context['message'] = check_plain($node->title);
  }
  if ($context['sandbox']['progress'] != count($nids)) {
    $context['finished'] = $context['sandbox']['progress'] / count($nids);
  }
}

/**
 * Batch API callback to clone invites from a round.
 *
 * @param array $invites
 *   The invite entites to clone.
 */
function delphi_clone_invites_operation($invites, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current'] = 0;
  }
  $chunk_size = 5; // Arbitrary.
  foreach (array_slice($invites, $context['sandbox']['progress']) as $original) {
    $original_wrapper = entity_metadata_wrapper('invite', $original);
    $invite = entity_create('invite', array(
      'type' => $original->type,
      'uid' => $original->uid,
      'invitee' => $original->invitee,
      'status' => $original->status,
    ));
    $invite_wrapper = entity_metadata_wrapper('invite', $invite);
    foreach (array_keys(field_info_instances('invite', $original->type)) as $field_name) {
      $invite_wrapper->{$field_name} = $original_wrapper->{$field_name}->value();
    }
    $invite_wrapper->field_round_node = $original_wrapper->field_round_node
      ->field_session
      ->current_round_node
      ->value();
    $invite_wrapper->save();
    $context['sandbox']['progress']++;
    $context['message'] = check_plain($original_wrapper->label());
  }
  if ($context['sandbox']['progress'] != count($invites)) {
    $context['finished'] = $context['sandbox']['progress'] / count($invites);
  }
}

/**
 * Form constructor for starting a round.
 *
 * Notify those and only those experts that have not yet received their
 * respective invitations
 *
 * @param $round_node
 *   The node object of the type "round" that needs to start.
 */
function delphi_start_round_form($form, &$form_state, $round_node) {
  $count_questions = db_query("
    SELECT COUNT(*)
    FROM {field_data_field_round_node}
    WHERE deleted = 0
    AND field_round_node_target_id = :round_nid
    AND entity_type = 'node' AND bundle = 'question'
  ", array(
    ":round_nid" => $round_node->nid,
  ))->fetchField();
  $form_state['invites'] = delphi_round_load_invites($round_node);
  $count_experts = count($form_state['invites']);

  if (!$count_questions) {
    drupal_set_message(t('It appears this round has no questions yet.'), 'warning');
    drupal_goto('node/' . $round_node->nid . '/round/questions');
  }
  if (!$count_experts) {
    drupal_set_message(t('It appears this round has no experts yet.'), 'warning');
    drupal_goto('node/' . $round_node->nid . '/round/experts');
  }

  $form['help'][] = array(
    '#markup' => '<p>' . t('The notification process is automated with Delphi. All you have to do is modify the sample message text and the system will send out a customized e-mail to each of your experts.') . '</p>',
  );
  $form['help'][] = array(
    '#markup' => '<p>' . t('Use the form below to edit notification message options.') . '<br>' . t('Tokens are available from the table below.') . '</p>',
  );
  $form['expert_invite_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#required' => TRUE,
    '#default_value' => variable_get('delphi_expert_invite_subject', t('Delphi Decision Aid session invitation')),
  );
  $form['expert_invite_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Message text'),
    '#required' => TRUE,
    '#default_value' => variable_get('delphi_expert_invite_body'),
  );
  $form['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array(
      'user',
      'profile',
      'invite',
      'node',
    ),
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send e-mails'),
  );
  $form['actions']['help'] = array(
    '#markup' => '<p><strong>'
      . t('By clicking "Send e-mails" button you will notify all newly added experts that this session is in progress.')
      . '<br>'
      . t('You will not be able to add new questions any more.')
      . '</strong><br>'
      . t('You can <a href="@url">go back now</a> if you feel that you need to make some changes to your session.', array(
        '@url' => '/node/' . $round_node->nid . '/round/experts',
      ))
      . '</p>',
  );
  return $form;
}

/**
 * Submit handler for the round starting form.
 */
function delphi_start_round_form_submit($form, &$form_state) {
  $round_node = $form_state['build_info']['args'][0];
  if (lock_acquire("delphi_batch_round_" . $round_node->nid)) {
    $batch = array(
      'operations' => array(),
      'title' => 'Notifying experts',
      'init_message' => t('Setting up'),
      'progress_message' => t('Working on @current of @total.'),
      'finished' => 'delphi_release_lock',
      'file' => drupal_get_path('module', 'delphi') . '/delphi.pages.inc',
    );
    foreach ($form_state['invites'] as $invite) {
      $batch['operations'][] = array('delphi_notify_expert', array($invite, $form_state));
    }
    if ($batch['operations']) {
      batch_set($batch);
    }
    $round_node->field_round_progress[LANGUAGE_NONE][0]['value'] = 'started';
    node_save($round_node);
  }
  else {
    drupal_set_message(t("Your other operation is not yet complete.  Please wait for it to finish then try again."), 'error');
  }
  $form_state['redirect'] = 'node/' . $form_state['build_info']['args'][0]->nid . '/round/experts';
}

/**
 * Batch API callback: Dispatches an email for an invite entity.
 *
 * @param $invite
 *   The invite entity.
 * @param $form_state
 *   The state of the form.
 */
function delphi_notify_expert($invite, $form_state, &$context) {
  // Do not notify if it has already been done.
  if (empty($invite->field_invite_status[LANGUAGE_NONE][0]['value'])) {
    $invite->field_invitation_email_subject[LANGUAGE_NONE][0]['value'] = $form_state['values']['expert_invite_subject'];
    $invite->field_invitation_email_body[LANGUAGE_NONE][0]['value'] = $form_state['values']['expert_invite_body'];
    $invite = invite_load($invite->iid, TRUE);
    $invite->sendInvite();
    $invite->field_invite_status[LANGUAGE_NONE][0]['value'] = 1;
    invite_save($invite);
    // As soon as the first expert is notified the round has started.
    $round_node = node_load($invite->field_round_node[LANGUAGE_NONE][0]['target_id']);
    if ($round_node->field_round_progress[LANGUAGE_NONE][0]['value'] !== 'started') {
      $round_node->field_round_progress[LANGUAGE_NONE][0]['value'] = 'started';
      node_save($round_node);
    }
  }
  $context['results']['round_node_nid'] = $invite->field_round_node[LANGUAGE_NONE][0]['target_id'];
}

/**
 * Form constructor for renotifying experts.
 *
 * Notify experts who have not answered all the questions in a round that they
 * need to do so.
 *
 * @param $round_node
 *   The node object of the type "round" that experts will be notified about.
 */
function delphi_renotify_round_form($form, &$form_state, $round_node) {
  $form['help'][] = array(
    '#markup' => '<p>' . t('Just like when you started your session, the notification process is fully automated. Customized reminders will only be sent to those of your experts who have not yet answered all the questions in the round.') . '</p>',
  );
  $form['help'][] = array(
    '#markup' => '<p>' . t('Use the form below to edit reminder message options.')
      . '<br>'
      . t('Tokens are available from the table below.')
      . '</p>',
  );

  $form['expert_renotify_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#required' => TRUE,
    '#default_value' => variable_get('delphi_expert_renotify_subject', t('Delphi Decision Aid reminder')),
  );
  $form['expert_renotify_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Message text'),
    '#required' => TRUE,
    '#default_value' => variable_get('delphi_expert_renotify_body'),
  );
  $form['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array(
      'user',
      'profile',
      'invite',
      'node',
    ),
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send e-mails'),
  );

  return $form;
}

/**
 * Submit handler for the round renotify form.
 */
function delphi_renotify_round_form_submit($form, &$form_state) {
  $round_node = $form_state['build_info']['args'][0];
  if (lock_acquire("delphi_batch_round_" . $round_node->nid)) {
    $batch = array(
      'operations' => array(),
      'title' => 'Reminding experts',
      'init_message' => t('Setting up'),
      'progress_message' => t('Working on @current of @total.'),
      'finished' => 'delphi_release_lock',
      'file' => drupal_get_path('module', 'delphi') . '/delphi.pages.inc',
    );
    foreach (delphi_round_load_invites_by_progress($round_node->nid, "not all") as $iid) {
      $batch['operations'][] = array('delphi_renotify_expert', array($iid, $form_state));
    }
    if ($batch['operations']) {
      batch_set($batch);
    }
  }
  else {
    drupal_set_message(t("Your other operation is not yet complete.  Please wait for it to finish then try again."), 'error');
  }
  drupal_set_message(format_plural(count($batch['operations']), "Sent 1 email.", "Sent @count emails."));
  $form_state['redirect'] = 'node/' . $round_node->nid . '/round/monitoring';
}

/**
 * Batch API callback: Remind an expert about a round.
 *
 * @param integer $iid
 *   The ID of the invite entity to which a reminder should be sent.
 * @param array $form_state
 *   The state of the form.
 */
function delphi_renotify_expert($iid, $form_state) {
  $invite = invite_load($iid);
  $invite->field_invitation_email_subject[LANGUAGE_NONE][0]['value'] = $form_state['values']['expert_renotify_subject'];
  $invite->field_invitation_email_body[LANGUAGE_NONE][0]['value'] = $form_state['values']['expert_renotify_body'];
  invite_save($invite);
  $invite = invite_load($invite->iid, TRUE);
  $invite->sendInvite();
  $context['results']['round_node_nid'] = $invite->field_round_node[LANGUAGE_NONE][0]['target_id'];
}

/**
 * Form constructor for terminating a round.
 *
 * @param $round_node
 *   The node object of the type "round" that will be terminated.
 *
 * @ingroup forms
 */
function delphi_terminate_round_form($form, &$form_state, $round_node) {
  return confirm_form(
    $form,
    t('Are you sure you want to terminate this round?'),
    'node/' . $round_node->nid . '/round/monitoring',
    t('No more responses will be accepted after the round is terminated, and you will be able to proceed to the results overview.'),
    t('Terminate'));
}

/**
 * Submit handler for round terminate form.
 */
function delphi_terminate_round_form_submit($form, &$form_state) {
  $round_node = $form_state['build_info']['args'][0];
  $round_node->field_round_progress[LANGUAGE_NONE][0]['value'] = 'complete';
  node_save($round_node);
  $form_state['redirect'] = 'node/' . $round_node->nid . '/round/results';
  delphi_notify_round_complete($round_node);
}

/**
 * Form constructor for terminating a session.
 *
 * @param $round_node
 *   The node object of the type "round" from which the session will be
 *   terminated.
 *
 * @ingroup forms
 */
function delphi_terminate_session_form($form, &$form_state, $round_node) {
  return confirm_form($form, t("Are you sure you want to finish this session?"), 'sessions', t("Are you sure you want to finish this session?"), t("Finish"));
}

/**
 * Submit handler for session terminating form.
 */
function delphi_terminate_session_form_submit($form, &$form_state) {
  $round_wrapper = entity_metadata_wrapper('node', $form_state['build_info']['args'][0]);
  $round_wrapper->field_session->field_session_progress = 1;
  $round_wrapper->field_session->save();
  delphi_notify_session_complete($round_wrapper->field_session->value());
  $form_state['redirect'] = 'sessions';
  drupal_set_message(t('Your session %session_name is now complete.', array(
    '%session_name' => $round_wrapper->field_session->title->value(),
  )) . '<br>' . t('The experts have been notified by e-mail.') . '<br>' . t('The results will be available for your future review until you delete this session from your list.'));
}
