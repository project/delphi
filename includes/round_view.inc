<?php

/**
 * @file
 * Functions for viewing a round.
 */

/**
 * Builds a render array for a round node.
 *
 * @param $node
 *   The round node.
 * @param $state
 *   The state of the round.  Possible values include "questions" (Question
 *   Designing) and "experts" (Expert selection).
 *
 * @return
 *   A render array suitable for drupal_render().
 */
function delphi_round_view($node, $state = NULL) {
  $session = node_load($node->field_session[LANGUAGE_NONE][0]['target_id']);
  $round = $node->field_round[LANGUAGE_NONE][0]['value'];
  $render = array('content' => array());
  $render['content']['header']['title'] = array(
    '#value' => t('Session name: @title', array(
      '@title' => $session->title,
    )),
    '#theme' => 'html_tag',
    '#tag' => 'h2',
  );
  if (isset($session->body)) {
    $render['content']['header']['description'] = field_view_field('node', $node, 'body', 'default');
  }
  $render['content']['header']['round'] = array(
    '#value' => t('Round #: @round', array(
      '@round' => $round,
    )),
    '#theme' => 'html_tag',
    '#tag' => 'h2',
  );
  if (isset($state)) {
    $render['content']['header']['state'] = array(
      '#value' => t('Round state: @state', array(
        '@state' => $state,
      )),
      '#theme' => 'html_tag',
      '#tag' => 'h2',
    );
  }
  $render['content']['round_links'] = array(
    '#markup' => views_embed_view('session_rounds', 'block'),
  );

  $query_parameter = drupal_get_destination();
  $query_parameter['session'] = $session->nid;
  $query_parameter['round'] = $node->nid;
  $items = array();
  if ($state == 'questions') {
    // Allow adding questions only if the round is not yet started.
    if ($node->field_round_progress[LANGUAGE_NONE][0]['value'] != 'started') {
      $items[] = l(t("New question"), "node/add/question/" . $session->nid, array(
        'query' => array($query_parameter),
      ));
    }
    if (delphi_node_menu_access_callback($node, 'start')) {
      $items[] = l(t("Start round"), "node/" . $node->nid . "/round/start");
    }
  }
  elseif ($state == 'experts') {
    $items[] = l(t("New expert"), "invite/add/invite_by_email/" . $session->nid, array(
      'query' => array($query_parameter),
    ));
    if (delphi_node_menu_access_callback($node, 'start')) {
      if ($node->field_round_progress[LANGUAGE_NONE][0]['value'] === 'not started') {
        $items[] = l(t("Start round"), "node/" . $node->nid . "/round/start");
      }
      else {
        $items[] = l(t("Notify new experts"), "node/" . $node->nid . "/round/start");
      }
    }
  }
  elseif ($state == 'monitoring') {
    if (delphi_node_menu_access_callback($node, 'renotify')) {
      $items[] = l(t("Send reminders"), "node/" . $node->nid . "/round/renotify");
    }
    if (delphi_node_menu_access_callback($node, 'terminate')) {
      $items[] = l(t("Terminate round"), "node/" . $node->nid . "/round/terminate");
    }
  }
  elseif ($state == 'results') {
    if (delphi_node_menu_access_callback($node, 'terminate-session')) {
      $items[] = l(t("Finish session"), "node/" . $node->nid . "/round/terminate-session");
    }
    if (delphi_create_round_access_callback($session)) {
      $items[] = l(t("Next round"), "node/" . $session->nid . '/session/new-round');
    }
  }
  $items[] = l(t("Back to session list"), "sessions");
  $items[] = l(t("Log out"), "user/logout");
  $render['content']['action_links'] = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => $items,
    '#access' => node_access('update', $node),
  );

  switch ($state) {
    case 'questions':
      break;
    case 'experts':
      $render['content']['experts_content'] = delphi_experts_content($node);
      break;
    case 'monitoring':
      $render['content']['monitoring_content'] = delphi_monitoring_content($node);
      break;
    case 'results':
      $render['content']['results_content'] = delphi_results_content($node);
      break;
  }

  return $render;
}

/**
 * Builds a render array for the experts tab of a round node.
 *
 * @param object $node
 *   The round node.
 *
 * @return array
 *   An array suitable for rendering.
 */
function delphi_experts_content($node) {
  $render['advice_header'] = array(
    '#value' => t('Advice on selecting experts'),
    '#theme' => 'html_tag',
    '#tag' => 'h3',
  );
  $render['advice_content'][] = array(
    '#value' => t('How many experts are needed?'),
    '#theme' => 'html_tag',
    '#tag' => 'h4',
  );
  $render['advice_content'][] = array(
    '#value' => t('Use at least five experts. It is unlikely that you would need as many as 20.'),
    '#theme' => 'html_tag',
    '#tag' => 'p',
  );
  $render['advice_content'][] = array(
    '#value' => t('Whom should you select as experts?'),
    '#theme' => 'html_tag',
    '#tag' => 'h4',
  );
  $render['advice_content'][] = array(
    '#value' => t('Experts need some expertise, but not a lot. The key thing is to find a heterogeneous group of experts. Thus, to select experts for a sales forecast of a new product, you might select from accountants, marketers and other managers in the company, retailers, financial analysts, people involved in the new product project, and people retired from the company.'),
    '#theme' => 'html_tag',
    '#tag' => 'p',
  );
  if ($node->field_round_progress[LANGUAGE_NONE][0]['value'] == 'started') {
    $render['advice_content'][] = array(
      '#value' => t("What are the <em>Notify new experts</em> and <em>Notify deleted experts</em> features of the system?"),
      '#theme' => 'html_tag',
      '#tag' => 'h4',
    );
    $render['advice_content'][] = array(
      '#value' => t("Delphi system has been updated to allow administrators to add or remove experts at all times."),
      '#theme' => 'html_tag',
      '#tag' => 'p',
    );
    $render['advice_content'][] = array(
      '#value' => t('When an administrator adds a new expert after the round is started the expert needs to be notified. When this is the case use the <em>Notify new experts</em> button.'),
      '#theme' => 'html_tag',
      '#tag' => 'p',
    );
    $render['advice_content'][] = array(
      '#value' => t('When an administrator deletes an existing expert after the round is started the expert needs to be notified. When this is the case use the <em>Notify deleted experts</em> button.'),
      '#theme' => 'html_tag',
      '#tag' => 'p',
    );
    $render['advice_content'][] = array(
      '#value' => t('What is <em>"In use"</em> status of an expert?'),
      '#theme' => 'html_tag',
      '#tag' => 'h4',
    );
    $render['advice_content'][] = array(
      '#value' => t('"In use" will show up in the status if an expert already started responding to the survey. When this is the case the expert cannot be removed.'),
      '#theme' => 'html_tag',
      '#tag' => 'p',
    );
  }
  return $render;
}

/**
 * Builds a render array for the "Process monitoring" tab of a round node.
 *
 * @param object $node
 *   The round node.
 *
 * @return array
 *   An array suitable for rendering.
 */
function delphi_monitoring_content($node) {
  $render['round_progress'][] = array(
    '#value' => t('Round progress'),
    '#theme' => 'html_tag',
    '#tag' => 'h4',
  );
  $render['round_progress'][] = array(
    '#value' => t('Round @progress', array(
      '@progress' => $node->field_round_progress[LANGUAGE_NONE][0]['value'],
    )),
    '#theme' => 'html_tag',
    '#tag' => 'p',
  );
  $render['round_progress'][] = array(
    '#value' => t('Number of responses received: @count of @all', array(
      '@count' => count(delphi_get_experts_responeded($node)),
      '@all' => delphi_round_load_invites($node, TRUE),
    )),
    '#theme' => 'html_tag',
    '#tag' => 'p',
  );
  if (delphi_round_all_responded($node)) {
    $render['round_progress'][] = array(
      '#value' => '<strong>' . t('All experts have responded.') . '</strong><br>' . t('The current round has not been terminated.  Click <strong><a href="@terminate_url">terminate round</a></strong> if you do not want to add any new experts.', array(
        '@terminate_url' => url('node/' . $node->nid . '/round/terminate'),
      )),
      '#theme' => 'html_tag',
      '#tag' => 'p',
    );
  }

  $render['manage_round'][] = array(
    '#value' => t('Manage round'),
    '#theme' => 'html_tag',
    '#tag' => 'h4',
  );
  $render['manage_round'][] = array(
    '#value' => t('Click <a href="@url">here</a> to send reminder e-mail to those experts who have not responded yet.', array(
      '@url' => url('node/' . $node->nid . '/round/renotify'),
    )),
    '#theme' => 'html_tag',
    '#tag' => 'p',
  );
  $render['manage_round'][] = array(
    '#value' => t('Click <a href="@url">here</a> to terminate this round now without waiting for other responses.', array(
      '@url' => url('node/' . $node->nid . '/round/terminate'),
    )),
    '#theme' => 'html_tag',
    '#tag' => 'p',
  );

  return $render;
}

/**
 * Builds a render array for the "Results review" tab of a round node.
 *
 * @param object $node
 *   The round node.
 *
 * @return array
 *   An array suitable for rendering.
 */
function delphi_results_content($node) {
  $round_wrapper = entity_metadata_wrapper('node', $node);
  $render['other_results'] = array(
    '#value' => t('Please use the links above to view results of other rounds.'),
    '#theme' => 'html_tag',
    '#tag' => 'p',
  );

  if ($round_wrapper->field_round_progress->value() == 'complete' && $round_wrapper->field_round->value() == $round_wrapper->field_session->field_current_round->value() && !$round_wrapper->field_session->field_session_progress->value()) {
    $render['manage_session'][] = array(
      '#value' => t("If you are satisfied with the results, you can finish this session now. All the data will be kept in the database until you delete this session from your session list. However, if you feel that you need more feedback you can start another round. This will enable you to edit existing quesions or add new ones as well as see how experts' opinions change over time. This is an important aspect of the Delphi process."),
      '#theme' => 'html_tag',
      '#tag' => 'p',
    );
    $render['manage_session'][] = array(
      '#theme' => 'item_list',
      '#type' => 'ul',
      '#items' => array(
        l(t('Finish this session'), 'node/' . $node->nid . '/session/terminate-session'),
        l(t('Proceed to next round'), 'node/' . $round_wrapper->field_session->getIdentifier() . '/session/new-round'),
      ),
    );
  }

  if ($round_wrapper->field_round_progress->value() != 'complete') {
    $render['incomplete'][] = array(
      '#value' => t('This round is not complete. <strong>Only Partial Results are available.</strong>'),
      '#theme' => 'html_tag',
      '#tag' => 'p',
    );
  }

  $question_ids = delphi_round_load_questions($node);
  if ($question_ids) {
    $question = arg(4);// node/%node/round/results/%
    if (!$question || !in_array($question, $question_ids)) {
      $question = end($question_ids);
      reset($question_ids);
    }
    $render['results'] = drupal_get_form('delphi_render_question_results', $question);
    $render['other_questions'] = array(
      '#title' => t('Select question to view results.'),
      '#type' => 'ul',
      '#items' => array(),
      '#theme' => 'item_list',
    );
    $question_number = 1;
    foreach ($question_ids as $qnid) {
      $render['other_questions']['#items'][] = l('[ ' . $question_number . ' ]', 'node/' . $node->nid . '/round/results/' . $qnid);
      $question_number++;
    }
  }
  return $render;
}
