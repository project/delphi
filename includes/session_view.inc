<?php

/**
 * @file
 * Functions for viewing a session.
 */

/**
 * Builds a render array for a session.
 *
 * @param $node
 *   The session node.
 * @param $state
 *   The aspect of the session to display.
 *
 * @return
 *   A render array suitable for drupal_render().
 */
function delphi_session_view($node, $state) {
  $render['content']['header']['title'] = array(
    '#value' => check_plain($node->title),
    '#theme' => 'html_tag',
    '#tag' => 'h2',
  );
  $current_round = delphi_round_node_load($node);
  $render['content']['round_links'] = array(
    '#markup' => views_embed_view('session_rounds', 'block', $current_round->nid),
  );

  switch ($state) {
    case 'results':
      $render['content']['header']['title']['#value'] = t('@title: overall session results', array(
        '@title' => $node->title,
      ));
      $render['content']['results'] = delphi_session_overall_results_page($node);
      break;
  }

  return $render;
}

/**
 * Page callback for displaying a session's overall results.
 *
 * @param $node
 *   The node object of the type "session".
 */
function delphi_session_overall_results_page($node) {
  $session = entity_metadata_wrapper('node', $node);
  $render['description_header'] = array(
    '#value' => t('Overall session results'),
    '#theme' => 'html_tag',
    '#tag' => 'h3',
  );
  $render['description_content'][] = array(
    '#value' => t('This page represents summarized statistics for every question used in the session.'),
    '#theme' => 'html_tag',
    '#tag' => 'p',
  );
  $render['description_content'][] = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => array(
      t('If a question was used in several rounds throughout the session, you will see the summary that takes into account all of the answers received in different rounds.'),
      t('If a question was used in a single round, the statistics here will be identical to those for that round.'),
    ),
  );

  if ($session->current_round_node->field_round_progress->value() == 'complete' && !$session->field_session_progress->value()) {
    $render['terminate_session_help'] = array(
      '#value' => t("If you are satisfied with the results, you can finish this session now. All the data will be kept in the database until you delete this session from your session list. However, if you feel that you need more feedback you can start another round. This will enable you to edit existing quesions or add new ones as well as see how experts' opinion changes over time. This is an important aspect of the Delphi process."),
      '#theme' => 'html_tag',
      '#tag' => 'p',
    );
    $render['manage_session'][] = array(
      '#theme' => 'item_list',
      '#type' => 'ul',
      '#items' => array(
        l(t('Finish this session'), 'node/' . $node->nid . '/session/terminate-session'),
        l(t('Proceed to next round'), 'node/add/round'),
      ),
    );
  }

  $question_map = delphi_session_load_questions($node);
  if ($question_map) {
    $question_nid = arg(4);// node/%node/session/results/%
    $question = entity_metadata_wrapper('node', $question_nid);
    if (!$question_nid || !$question->value()) {
      foreach ($question_map as $type => $by_type) {
        foreach ($by_type['by_title'] as $title => $questions) {
          // Get the last questions from the last type.
        }
      }
    }
    else {
      $questions = $question_map[$question->field_question_type->value()]['by_title'][$question->title->value()];
    }
    $render['results'] = drupal_get_form('delphi_render_question_results', $questions);
    $render['other_questions'] = array(
      '#title' => t('Select question to view results.'),
      '#type' => 'ul',
      '#items' => array(),
      '#theme' => 'item_list',
    );
    $question_number = 1;
    foreach ($question_map as $type => $by_type) {
      foreach ($by_type['by_title'] as $title => $qs) {
        $render['other_questions']['#items'][] = l('[ ' . $question_number . ' ]', 'node/' . $node->nid . '/session/results/' . $qs[0]->nid);
        $question_number++;
      }
    }
  }
  return $render;
}

