<?php

/**
 * @file
 * Ugly functions roughly ported from Delphi version 2.
 */

/**
 * Renders a question's results.
 *
 * @param array $form_state
 *   The state of the form containg the question number and question.
 * @param str $detail_level
 *   The level of detail to show.  Possible values are "brief",
 *   "detailed-empty", and "detailed-all".
 *
 * @return array
 *   Render array for table with statistics in it.
 */
function session_results_getQuestionStatsHTML($form_state, $detail_level) {
  $question_number = $form_state['question_number'];
  $question = $form_state['question'];

  $responses = array();
  module_load_include('inc', 'webform', 'includes/webform.submissions');
  foreach ($form_state['questions'] as $q) {
    $responses = array_merge($responses, webform_get_submissions($q->nid));
  }

  if ($question->field_question_type->value() == 'ranking') {
    $stats_array = session_results_getRankingStatsArray($responses);
    return getRankingStatsTableHTML($question, $stats_array);
  }
  elseif ($question->field_question_type->value() == 'scaled') {
    $min_ans = $question->field_scale_left_value->value();
    $max_ans = $question->field_scale_right_value->value();
    $detail_level = 'detailed-all';
  }
  else {
    $min_ans = getMaxMinAnswer($responses, "min");
    $max_ans = getMaxMinAnswer($responses, "max");
  }

  if ($detail_level == 'detailed-empty') {
    // Try to determine optimal size of empty ranges based on average gap
    // between answers.
    $average_gap = session_results_getAverageGapSize($responses, $min_ans, $max_ans);
    return session_results_getDetailedStatsTableHTML($question, $responses, round($average_gap / 2), $min_ans, $max_ans);
  }
  elseif ($detail_level == 'detailed-all') {
    return session_results_getDetailedStatsTableHTML($question, $responses, 1, $min_ans, $max_ans);
  }
  else {
    return session_results_getStatsTableHTML($question, $responses, $max_ans);
  }
}

/**
 * Converts submissions to a ranking question to a statistics array.
 *
 * @param array $responses
 *   An array of webform submissions.
 *
 * @return array
 *   An associative array of statistics where each numbered key is the 1-indexed
 *   number of the ranking item and each value is an associative array
 *   containing:
 *   - av_rank: The average rank assigned by experts to the item.
 *   - best_rank: The highest rank any expert has assigned to the item.
 *   - work_rank: The lowest rank any expert has assigned to the item.
 *   - ranked_first: The number of items the item has been chosen as the best.
 *
 *   Additional return keys include:
 *   - _no_opinion_answers: The number of responses specifying "no opinion".
 *   - _total_answers: The number of submissions.  Note that it is possible that
 *     the number of ranked answers plus the number in _no_opinion_answers may
 *     not be equal to _total_answers if any submissions were made before
 *     any of the current ranking items were added or are in any other way
 *     malformmed.
 *   - _malformed_answers: The number of ansers that are neither complete nor
 *     "no opinion".
 */
function session_results_getRankingStatsArray($responses) {
  // An array that has IDs of ranked items as its indeces and lists of ranks
  // assiged to corresponding item by different experts as its elements.
  $index_ranking_array = array();

  $total_answers = 0;
  $no_opinion_count = 0;
  $malformed_count = 0;
  $previous_nid = FALSE;

  foreach ($responses as $response) {
    if ($response->nid != $previous_nid) {
      $node = node_load($response->nid);
      $no_opinion_cid = webform_get_cid($node, "no_opinion", 0);
      $previous_nid = $response->nid;
    }
    if ($no_opinion_cid && isset($response->data[$no_opinion_cid][0]) && $response->data[$no_opinion_cid][0]) {
      $no_opinion_count++;
    }
    else {
      $malformed = TRUE;
      foreach ($response->data as $cid => $component) {
        if (strpos($node->webform['components'][$cid]['form_key'], "items_to_rank_") !== FALSE && $component[0] >= 0) {
          $indexed_ranking_array[substr($node->webform['components'][$cid]['form_key'], strlen("items_to_rank_")) + 1][] = $component[0];
        }
        $malformed = FALSE;
      }
      if ($malformed) {
        $malformed_count++;
      }
    }
    $total_answers++;
  }

  $stats_array = array();
  foreach ($indexed_ranking_array as $ranked_item => $ranks) {
    $item_stats = array();
    sort($ranks);
    $item_stats['av_rank'] = round((array_sum($ranks) / count($ranks)), 1);
    $item_stats['best_rank'] = $ranks[0];
    $item_stats['worst_rank'] = $ranks[count($ranks) - 1];

    $count = 0;
    foreach ($ranks as $cur_rank) {
      if ($cur_rank == 1) {
        $count++;
      }
    }
    $item_stats["ranked_first"] = $count;

    $stats_array[$ranked_item] = $item_stats;
  }
  ksort($stats_array);

  $stats_array["_no_opinion_answers"] = $no_opinion_count;
  $stats_array["_malformed_answers"] = $malformed_count;
  $stats_array["_total_answers"] = $total_answers;

  return $stats_array;
}

/**
 * Renders a table containing statistics about a ranking question.
 *
 * @param EntityMetadataWrapper $question
 *   The wrapper of a question node of the type "ranking" with statistics to be
 *   rendered.
 * @param array $stats
 *   An array of statistics created by session_results_getRankingStatsArray().
 *
 * @see session_results_getRankingStatsArray()
 *
 * @return array
 *   Returns a render array for a ranking question statistics table.
 */
function getRankingStatsTableHTML($question, $stats_array) {
  $header = array();
  $rows = array();
  $attributes = array();

  $rows[0][0] = array(
    'data' => '<strong>' . t('Question') . ':</strong> ' . check_plain($question->title->value()),
    'colspan' => 6,
  );
  $items_rows = array();
  $items_caption = '<strong>' . t('Items ranked') . ':</strong>';
  foreach ($question->value()->webform['components'] as $component) {
    if (strpos($component['form_key'], "items_to_rank_") !== FALSE) {
      $items_rows[] = array(
        webform_filter_xss($component['name']),
        webform_filter_descriptions($component['extra']['description']),
      );
    }
  }
  $rows[0][0]['data'] .= theme('table', array(
    'rows' => $items_rows,
    'caption' => $items_caption,
  ));

  $rows[] = array(
    t('Item'),
    t('Average rank'),
    t('Best rank'),
    t('Worst rank'),
    array(
      'data' => t('Number of times ranked #1'),
      'colspan' => 2,
    ),
  );

  $bar_color = 0;
  foreach ($stats_array as $ranked_item_id => $stats) {
    if ($ranked_item_id == "_total_answers") {
      $rows[] = array(
        array('data' => t('Total number of answers') . ':', 'colspan' => 2),
        array('data' => $stats, 'colspan' => 4),
      );
    }
    elseif ($ranked_item_id == "_no_opinion_answers") {
      $rows[] = array(
        array(
          'data' => t('Number of "no opinion" answers') . ':',
          'colspan' => 2,
        ),
        array('data' => $stats, 'colspan' => 4),
      );
    }
    elseif ($ranked_item_id == "_malformed_answers") {
      if ($stats) {
        $rows[] = array(
          array(
            'data' => t('Number of malformed answers') . ':',
            'colspan' => 2,
          ),
          array('data' => $stats),
        );
      }
    }
    else {
      $percentage = round(($stats["ranked_first"] / $stats_array["_total_answers"]) * 100);
      $cids = webform_get_cid($question->value(), 'items_to_rank_' . ($ranked_item_id - 1));
      $rows[] = array(
        '<strong>' . webform_filter_xss($question->value()->webform['components'][$cids[0]]['name']) . '</strong>',
        $stats["av_rank"],
        $stats["best_rank"],
        $stats["worst_rank"],
        $stats["ranked_first"] . " ($percentage%)",
        getColorBar($percentage, chart_colors($bar_color)),
      );

      $bar_color++;
      if ($bar_color == count(chart_colors())) {
        $bar_color = 0;
      }
    }
  }

  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => $attributes,
  );
}

/**
 * Calculates the average distance between two answers in numerical sequence.
 *
 * @param array $submissions
 *   An array of webform submissions to the given question.
 * @param int|float
 *   The minimum value submitted.
 * @param int|float
 *   The maximum value submitted.
 *
 * @return int
 *   The average difference between two consecutive answers.
 */
function session_results_getAverageGapSize($submissions, $min_ans, $max_ans) {
  if ($min_ans == $max_ans) {
    return 0;
  }
  $total_gap = 0;
  $last_answer = $min_ans;

  $answers = array();
  $previous_nid = FALSE;
  foreach ($submissions as $submission) {
    if ($submission->nid != $previous_nid) {
      $node = node_load($submission->nid);
      $cid = webform_get_cid($node, "answer", 0);
      $previous_nid = $submission->nid;
    }
    if (isset($submission->data[$cid][0])) {
      $answers[] = $submission->data[$cid][0];
    }
  }
  sort($answers);
  foreach ($answers as $answer) {
    $total_gap += $answer - $last_answer;
    $last_answer = $answer;
  }
  return $total_gap / (count($answers) - 1);
}

/**
 * Renders a table containing detailed statistics about a question.
 *
 * @param EntityMetadataWrapper $question
 *   The question containing information about the webform components referenced
 *   in the submissions.
 * @param array $submissions
 *   An array of webform submissions to the question(s).
 * @param int|float $average_gap
 *   The average difference between one answer the next in numerical order.
 * @param int|float $min_ans
 *   The lowest answer submitted.
 * @param int|float $max_ans
 *   The highest answer submitted.
 *
 * @return array
 *   Returns a render array for a question statistics table.
 */
function session_results_getDetailedStatsTableHTML($question, $submissions, $average_gap, $min_ans, $max_ans) {
  $header = array();
  $rows = array();
  $attributes = array();

  $rows[0][0] = array(
    'data' => '<strong>' . t('Question') . ':</strong> ' . check_plain($question->title->value()),
    'colspan' => 4,
  );

  if ($question->field_question_type->value() == 'scaled') {
    $rows[0][0]['data'] .= '<p><strong>' . t("Scale") . ":</strong> " . check_plain($question->field_scale_left_value->value()) . " (" . check_plain($question->field_scale_left_label->value()) . ") " . t("to") . " " . check_plain($question->field_scale_right_value->value()) . " (" . check_plain($question->field_scale_right_label->value()) . ") <br>";
  }

  if ($question->field_show_bounds->value()) {
    $rows[] = array(
      array('rowspan' => 2, 'data' => t("Answer")),
      array('colspan' => 2, 'data' => t("Experts' confidence")),
      array('rowspan' => 2, 'data' => '&nbsp;'),
    );
    $rows[] = array(t('Lower bound'), t('Upper bound'));
  }
  else {
    $rows[] = array(
      t("Answer"),
      array('data' => "&nbsp;", "colspan" => 2),
    );
  }

  $bar_color = 0;
  $i = $min_ans;

  if ($average_gap == 0) {
    // All answers are identical or there is only one answer.
    $answers = $submissions;
    $previous_nid = FALSE;
    foreach ($answers as $answer) {
      if ($answer->nid != $previous_nid) {
        $webform = entity_metadata_wrapper('node', $answer->nid);
        $answer_cid = webform_get_cid($webform->value(), "answer", 0);
        $confidence_lb_cid = webform_get_cid($webform->value(), "confidence_lb", 0);
        $confidence_ub_cid = webform_get_cid($webform->value(), "confidence_ub", 0);
        $previous_nid = $answer->nid;
      }
      if (isset($answer->data[$answer_cid])) {
        $row = array(
          '<strong>' . check_plain($answer->data[$answer_cid][0]) . '</strong>',
        );
        if ($webform->field_show_bounds->value()) {
          $confidence_lb_cid = webform_get_cid($webform->value(), "confidence_lb", 0);
          $confidence_ub_cid = webform_get_cid($webform->value(), "confidence_ub", 0);
          $row[] = check_plain($answer->data[$confidence_lb_cid][0]);
          $row[] = check_plain($answer->data[$confidence_ub_cid][0]);
          $row[] = getColorBar(100, chart_colors($bar_color));
        }
        else {
          $row[] = array(
            'colspan' => 2,
            getColorBar(100, chart_colors($bar_color)),
          );
        }
        $rows[] = $row;

        $bar_color++;
        if ($bar_color == count(chart_colors())) {
          $bar_color = 0;
        }
      }
    }
  }
  else {
    // There are different answers.
    while ($i <= $max_ans) {
      $answers = array();
      $previous_nid = FALSE;
      foreach ($submissions as $submission) {
        if ($previous_nid != $submission->nid) {
          $webform = node_load($submission->nid);
          $answer_cid = webform_get_cid($webform, "answer", 0);
          $no_opinion_cid = webform_get_cid($webform, "no_opinion", 0);
          $previous_nid = $submission->nid;
        }
        if (empty($submission->data[$no_opinion_cid][0]) && $submission->data[$answer_cid][0] >= $i && $submission->data[$answer_cid][0] < ($i + $average_gap)) {
          $answers[$submission->data[$answer_cid][0]] = $submission;
        }
      }

      if (!empty($answers)) {
        $previous_nid = FALSE;
        foreach ($answers as $answer) {
          if ($previous_nid != $submission->nid) {
            $webform = node_load($submission->nid);
            $answer_cid = webform_get_cid($webform, "answer", 0);
            $no_opinion_cid = webform_get_cid($webform, "no_opinion", 0);
            $confidence_lb_cid = webform_get_cid($webform, "confidence_lb", 0);
            $confidence_ub_cid = webform_get_cid($webform, "confidence_ub", 0);
            $previous_nid = $submission->nid;
          }
          $percentage = round(($answer->data[$answer_cid][0] / $max_ans) * 100);
          $row = array(
            '<strong>' . $answer->data[$answer_cid][0] . '</strong>',
          );
          if ($question->field_show_bounds->value()) {
            $row[] = $answer->data[$confidence_lb_cid][0];
            $row[] = $answer->data[$confidence_ub_cid][0];
          }
          $row[] = getColorBar($percentage, chart_colors($bar_color));
          $rows[] = $row;

          $bar_color++;
          if ($bar_color == count(chart_colors())) {
            $bar_color = 0;
          }
          $i = round($answer->data[$answer_cid][0] + 1);
        }
      }
      else {
        if ($average_gap != 1) {
          $rows[] = array(t("No answers"));
        }
        else {
          $rows[] = array($i);
        }

        if ($question->field_show_bounds->value()) {
          $rows[] = array(t('N/A'));
        }
        $rows[] = array(getColorBar(0, chart_colors($bar_color)));

        $bar_color++;
        if ($bar_color == count(chart_colors())) {
          $bar_color = 0;
        }

        $i += $average_gap;
      }
    }
  }

  // Outout "no opinion" answers in the table if any.
  $answers = array();
  $previous_nid = FALSE;
  foreach ($submissions as $submission) {
    if ($submission->nid != $previous_nid) {
      $webform = node_load($submission->nid);
      $answer_cid = webform_get_cid($webform, "answer", 0);
      $no_opinion_cid = webform_get_cid($webform, "no_opinion", 0);
      $previous_nid = $submission->nid;
    }
    if (!empty($submission->data[$no_opinion_cid][0])) {
      $answers[] = $submission;
    }
  }
  foreach ($answers as $answer) {
    $rows[] = array(t('No opinion'), array('data' => t('N/A'), 'colspan' => 2));
  }

  // Outout stats in the table.
  $stats = session_results_getResultsStats($question);
  $colspan = $question->field_show_bounds->value() ? 3 : 1;
  $rows[] = array(
    array('data' => t('Total number of answers') . ':', 'colspan' => $colspan),
    $stats['num_answers'],
  );
  $rows[] = array(
    array('data' => t('Mean') . ':', 'colspan' => $colspan),
    $stats['average'],
  );
  $rows[] = array(
    array('data' => t('Standard deviation') . ':', 'colspan' => $colspan),
    $stats['stddev'],
  );
  $rows[] = array(
    array('data' => t('Median') . ':', 'colspan' => $colspan),
    $stats['median'],
  );

  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => $attributes,
  );
}

/**
 * Gets statistics for a question.
 *
 * @param EntityMetadataWrapper $question
 *   The the wrapper for the question node of interest.
 *
 * @return array
 *   An associative array of statistics containing:
 *   - num_answers: The number of responses received to the question.
 *   - average: The average value of all the answers or "N/A" if no answers were
 *     received.
 *   - stddev: The standard deviation among the answers or "N/A" if no answers
 *     were received.
 *   - median: The median answer or "N/A" if no answers were received.
 */
function session_results_getResultsStats($question) {
  $responses = webform_get_submissions($question->getIdentifier());
  $answer_cid = webform_get_cid($question->value(), "answer", 0);
  $no_opinion_cid = webform_get_cid($question->value(), "no_opinion", 0);

  $numeric_answers = array();
  foreach ($responses as $response) {
    if (!empty($response->data[$answer_cid])) {
      $numeric_answers[] = $response->data[$answer_cid][0];
    }
  }

  $stats['num_answers'] = count($responses);
  if (count($numeric_answers)) {
    $stats['average'] = round((array_sum($numeric_answers) / count($numeric_answers)), 2);
    $stats['stddev'] = round(stats_standard_deviation($numeric_answers), 2);
    sort($numeric_answers);
    if (count($numeric_answers) % 2 == 1) {
      $stats['median'] = $numeric_answers[floor(count($numeric_answers) / 2)];
    }
    else {
      $index = count($numeric_answers) / 2;
      $stats['median'] = round(($numeric_answers[$index] + $numeric_answers[$index - 1]) / 2, 2);
    }
  }
  else {
    $stats['average'] = t('N/A');
    $stats['stddev'] = t('N/A');
    $stats['median'] = t('N/A');
  }

  return $stats;
}

/**
 * Renders a table containing brief statistics about a question.
 *
 * @param EntityMetadataWrapper $question
 *   The question containing information about the webform components referenced
 *   in the submissions.
 * @param array $submissions
 *   An array of webform submissions from which to gather statistics.
 * @param int|float $max_ans
 *   The highest answer submitted.
 *
 * @return array
 *   Returns a render array for a question statistics table.
 */
function session_results_getStatsTableHTML($question, $submissions, $max_ans) {
  $header = array();
  $rows = array();
  $attributes = array();

  $rows[0][0] = array(
    'data' => '<strong>' . t('Question') . ':</strong> ' . check_plain($question->title->value()),
    'colspan' => 5,
  );

  if ($question->field_question_type->value() == 'scaled') {
    $rows[0][0]['data'] .= '<p><strong>' . t("Scale") . ":</strong> " . check_plain($question->field_scale_left_value->value()) . " (" . check_plain($question->field_scale_left_label->value()) . ") " . t("to") . " " . check_plain($question->field_scale_right_value->value()) . " (" . check_plain($question->field_scale_right_label->value()) . ") <br>";
  }

  if ($question->field_show_bounds->value()) {
    $rows[] = array(
      array('rowspan' => 2, 'data' => t('Answer')),
      array('colspan' => 2, 'data' => t("Experts' confidence")),
      array('rowspan' => 2, 'data' => t('Number of answers')),
      array('rowspan' => 2, 'data' => '&nbsp;'),
    );
    $rows[] = array(t('Lower bound'), t('Upper bound'));
  }
  else {
    $rows[] = array(t("Answer"), '&npsp;');
  }

  $bar_color = 0;
  $count_answers = array();
  $confidence_lb_total = array();
  $confidence_ub_total = array();
  $previous_nid = FALSE;
  // Populate the arrays.
  foreach ($submissions as $submission) {
    if ($previous_nid != $submission->nid) {
      $node = node_load($submission->nid);
      $answer_cid = webform_get_cid($node, "answer", 0);
      $no_opinion_cid = webform_get_cid($node, "no_opinion", 0);
      $confidence_lb_cid = webform_get_cid($node, "confidence_lb", 0);
      $confidence_ub_cid = webform_get_cid($node, "confidence_ub", 0);
      $previous_nid = $submission->nid;
    }

    if (empty($submission->data[$no_opinion_cid][0])) {
      $answer = $submission->data[$answer_cid][0];
      if (!isset($count_answers[$answer])) {
        $count_answers[$answer] = 0;
        $confidence_lb_total[$answer] = 0;
        $confidence_ub_total[$answer] = 0;
      }
      $count_answers[$answer]++;
      $confidence_lb_total[$answer] += $submission->data[$confidence_lb_cid][0];
      $confidence_ub_total[$answer] += $submission->data[$confidence_ub_cid][0];
    }
  }
  // Use the arrays to create rows in the table.
  foreach ($count_answers as $answer => $count) {
    $row = array();
    $row[] = '<strong>' . $answer . '</strong>';
    if ($question->field_show_bounds->value()) {
      $row[] = round($confidence_lb_total[$answer] / count($confidence_lb_total), 2);
      $row[] = round($confidence_ub_total[$answer] / count($confidence_ub_total), 2);
    }
    $row[] = $count;
    $percentage = round(($answer / $max_ans) * 100);
    $row[] = getColorBar($percentage, chart_colors($bar_color));
    $rows[] = $row;

    $bar_color++;
    if ($bar_color == count(chart_colors())) {
      $bar_color = 0;
    }
  }

  // Output "no opinion" answers in the table if any.
  $answers = array();
  $previous_nid = FALSE;
  foreach ($submissions as $submission) {
    if ($previous_nid != $submission->nid) {
      $node = node_load($submission->nid);
      $no_opinion_cid = webform_get_cid($node, "no_opinion", 0);
      $previous_nid = $submission->nid;
    }

    if (!empty($submission->data[$no_opinion_cid][0])) {
      $answers[] = $submission;
    }
  }
  if ($answers) {
    $rows[] = array(
      t('No opinion'),
      array('data' => t('N/A'), 'colspan' => 2),
      count($answers),
      t('N/A'),
    );
  }

  // Output stats in the table.
  $stats = session_results_getResultsStats($question);
  $rows[] = array(
    array('colspan' => 3, 'data' => t('Total number of answers') . ':'),
    array('colspan' => 2, 'data' => $stats['num_answers']),
  );
  $rows[] = array(
    array('colspan' => 3, 'data' => t('Mean') . ':'),
    array('colspan' => 2, 'data' => $stats['average']),
  );
  $rows[] = array(
    array('colspan' => 3, 'data' => t('Standard deviation') . ':'),
    array('colspan' => 2, 'data' => $stats['stddev']),
  );
  $rows[] = array(
    array('colspan' => 3, 'data' => t('Median') . ':'),
    array('colspan' => 2, 'data' => $stats['median']),
  );

  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => $attributes,
  );
}

/**
 * Gets the maximum and minimum values submitted.
 *
 * @param array $submissions
 *   An array of webform submissions.
 * @param str $extreme
 *   Which value to retrieve.  Possible values are "min" and "max".
 *
 * @return mixed
 *   The maximum or minimum answer or FALSE if no answer was found.
 */
function getMaxMinAnswer($submissions, $extreme) {
  $value = FALSE;
  foreach ($submissions as $submission) {
    if (!isset($question) || $question->nid != $submission->nid) {
      $question = node_load($submission->nid);
      $cid = webform_get_cid($question, "answer", 0);
    }
    if (isset($submission->data[$cid][0])) {
      if ($value === FALSE) {
        $value = $submission->data[$cid][0];
      }
      else {
        switch ($extreme) {
          case "min":
            if ($submission->data[$cid][0] < $value) {
              $value = $submission->data[$cid][0];
            }
            break;
          case "max":
            if ($submission->data[$cid][0] > $value) {
              $value = $submission->data[$cid][0];
            }
            break;
        }
      }
    }
  }
  return $value;
}

/**
 * Renders a percentage bar.
 *
 * @param int|float $percentage
 *   The percentage of the bar that should be the color of $color.
 * @param string $color
 *   The color of the bar using any value suitable for CSS.
 */
function getColorBar($percentage, $color) {
  $attributes = array(
    "style" => "background-color: #555555; width: 100%;",
  );
  $rows = array(array(
    array(
      "style" => "width: " . $percentage . "%; background-color: " . $color . ";",
      "data" => '&nbsp;',
    ),
    array(
      "style" => "width: " . (100 - $percentage) . "%;",
      "data" => "",
    ),
  ));
  return theme('table', array('attributes' => $attributes, 'rows' => $rows));
}

/**
 * Returns a color or all defined colors.
 *
 * @param int $index
 *   (optional) The index of the color to return.  If blank all colors will be
 *   returned.
 *
 * @return str|array
 *   The color specified by $index or all colors if $index is omitted.
 */
function chart_colors($index = NULL) {
  $chart_colors = array(
    "#FFFF99",
    "#CCCC66",
    "#FF9900",
    "#66CC99",
    "#FFCCCC",
    "#99CCCC",
    "#CC99CC",
    "#6699FF",
    "#CCCCFF",
    "#9999CC",
    "#CC99FF",
    "#99CCFF",
    "#CC9999",
    "#99FFCC",
    "#FFCC99",
    "#CCFFCC",
    "#FFCC66",
  );
  if (isset($index)) {
    return $chart_colors[$index];
  }
  return $chart_colors;
}

/**
 * Renders comments for from experts on a question.
 *
 * @param EntityMetadataWrapper $question
 *   The question for which to retrieve answers and comments.
 * @param str $label
 *   The label to apply the output.
 *
 * @return array
 *   A render array of comments.
 */
function session_results_getComments($question, $label) {
  $render = array();

  module_load_include('inc', 'webform', 'includes/webform.submissions');
  $submissions = webform_get_submissions($question->getIdentifier());
  $answer_cid = webform_get_cid($question->value(), 'answer', 0);
  $comment_cid = webform_get_cid($question->value(), 'comment', 0);

  foreach ($submissions as $submission) {
    if (!empty($submission->data[$comment_cid][0])) {
      switch ($question->field_question_type->value()) {
        case 'ranking':
          $rank1cids = webform_cid($question->value(), 'items_to_rank_0');
          $rank1cid = $rank1cids[0];
          $render[] = array(
            '#theme' => 'table',
            '#caption' => '<em>' . t('Expert ranked #1') . ':</em',
            '#rows' => array(array(
              '&nbsp;',
              webform_filter_xss($question->value()->webform['components'][$rank1cid]['name']),
            )),
          );
          $render[] = array(
            '#theme' => 'table',
            '#caption' => '<em>' . t("Expert's comment") . ':</em>',
            '#rows' => array(array(
              '&nbsp;',
              check_plain($submission->data[$comment_cid][0]),
            )),
          );
          break;
        case 'text':
          $render[] = array(
            '#theme' => 'table',
            '#caption' => '<em>' . t("Expert's comment") . ':</em>',
            '#rows' => array(array(
              '&nbsp;',
              check_plain($submission->data[$comment_cid][0]),
            )),
          );
          break;
        default:
          $render[] = array(
            '#theme' => 'table',
            '#caption' => '<em>' . t("Expert's answer") . ':</em>',
            '#rows' => array(array(
              '&nbsp;',
              check_plain($submission->data[$answer_cid][0]),
            )),
          );
          $render[] = array(
            '#theme' => 'table',
            '#caption' => '<em>' . t("Expert's comment") . ':</em>',
            '#rows' => array(array(
              '&nbsp;',
              check_plain($submission->data[$comment_cid][0]),
            )),
          );
          break;
      }
    }
  }

  if (!$render) {
    return array(
      '#markup' => t('There are no expert comments for this question.'),
    );
  }
  $render['#prefix'] = '<h3>' . $label . '</h3>';
  return $render;
}

/**
 * Gets the question number from an array of questions.
 *
 * @param array $questions
 *   An array of questions with the same title.
 *
 * @return int
 *   The question number of the given questions, counting starting at 1.
 */
function admin_session_getCurrentQuestionOverallNum($questions) {
  $question = entity_metadata_wrapper('node', $questions[0]);
  $question_map = delphi_session_load_questions($question->field_session->value());
  $question_number = 1;
  foreach ($question_map as $type => $by_type) {
    foreach ($by_type['by_title'] as $title => $questions) {
      if ($title == $question->title->value()) {
        return $question_number;
      }
      $question_number++;
    }
  }
}
