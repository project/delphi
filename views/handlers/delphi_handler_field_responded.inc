<?php

/**
 * @file
 * Views field handler for indicating if an email address has responded.
 */

/**
 * A handler for indicating the response status of an email address in a round.
 *
 * In order for this to work there must be a token for an email address in the
 * alter text and the checkbox for altering the text must be checked.
 */
class delphi_handler_field_responded extends views_handler_field {
  /**
   * {@inheritdoc}
   */
  function query() {}

  function render($values) {
    return $this->options['alter']['text'];
  }

  /**
   * {@inheritdoc}
   */
  function render_altered($alter, $tokens) {
    $value = parent::render_altered($alter, $tokens);
    if ($account = user_load_by_mail($value)) {
      $next_question = delphi_get_next_unanswered_question_id($this->view->args[0], $account);
      $answered = !$next_question;
    }
    else {
      $answered = FALSE;
    }
    return $answered ? t('Yes') : '<strong>' . t('No') . '</strong>';
  }
}
