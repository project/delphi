<?php

/**
 * @file
 * Views integration for Delphi.
 */

/**
 * Implements hook_views_data().
 */
function delphi_views_data() {
  $data['invite']['delphi_responded'] = array(
    'title' => t('Responded'),
    'help' => t('Indicates if an invited expert has responded to all questions in a round.'),
    'field' => array(
      'handler' => 'delphi_handler_field_responded',
      'click sortable' => FALSE,
    ),
  );
  return $data;
}

/**
 * Implements hook_views_query_alter().
 *
 * Ignore webform submissions by other users on the questions view (expert
 * display).  The webform_submissions table is used only to determine if the
 * current user has answered the question.
 */
function delphi_views_query_alter(&$view, &$query) {
  // Alter the expert display of the questions block.
  if ($view->name == 'questions' && $view->current_display == 'block_2') {
    $query->table_queue['webform_submissions_node']['join']->extra[] = 'webform_submissions_node.uid = ' . $GLOBALS['user']->uid;
  }
}

/**
 * Implements hook_views_pre_build().
 */
function delphi_views_pre_build(&$view) {
  if ($view->name == 'rounds' && $view->current_display == 'entityreference_1') {
    if (isset($view->argument['field_session_target_id'])) {
      if (empty($view->argument['field_session_target_id']->value)) {
        $question = menu_get_object();
        if (is_object($question) && $question->type == 'question') {
          $session_nid = $question->field_session[LANGUAGE_NONE][0]['target_id'];
          // Sometimes this is unnecessary.
          $view->args[] = $session_nid;
        }
      }
    }
  }
}

/**
 * Implements hook_views_pre_render().
 *
 * Hide from the view of existing users those that have already been invited to
 * the round.
 */
function delphi_views_pre_render(&$view) {
  if ($view->name == 'invite_existing_user') {
    $round = node_load($view->args[0]);
    foreach ($view->result as $delta => $row) {
      $exists = db_query("
        SELECT email.field_invitation_email_address_value
        FROM {field_data_field_invitation_email_address} AS email
        INNER JOIN {field_data_field_round_node} AS round
        ON round.entity_type = 'invite' AND round.entity_id = email.entity_id
        WHERE email.entity_type = 'invite'
        AND email.entity_id = round.entity_id
        AND field_invitation_email_address_value = :mail
        AND round.field_round_node_target_id = :round_nid
        AND round.deleted = 0
        AND email.deleted = 0
      ", array(
        ":round_nid" => $round->nid,
        ":mail" => $row->users_mail,
      ))->fetchField();
      if ($exists) {
        unset($view->result[$delta]);
      }
    }
  }
  elseif ($view->name == 'session_rounds') {
    $round = entity_metadata_wrapper('node', $view->args[0]);
    if ($round->field_session->field_current_round->value() != 1 && delphi_session_overall_results_access_callback($round->field_session->value(), 'results')) {
      $view->attachment_after = l(t('Overall session results'), 'node/' . $round->field_session->getIdentifier() . '/session/results');
    }
  }
  elseif ($view->name == 'expert_sessions') {
    foreach ($view->result as $result) {
      if (isset($result->field_field_round_progress[0]['raw']['value'])) {
        if ($result->field_field_round_progress[0]['raw']['value'] == 'not started') {
          $result->field_field_round_progress[0]['rendered']['#markup'] = t('No');
        }
        else {
          $result->field_field_round_progress[0]['rendered']['#markup'] = t('Yes');
        }
      }
    }
  }
  elseif ($view->name == 'questions') {
    foreach ($view->result as $result) {
      // Only non-text-only questions.
      if ($result->field_field_question_type[0]['raw']['value'] != 'text') {
        if (!empty($result->field_field_no_opinion[0]['raw']['value'])) {
          // Set no opinion text.
          $result->field_field_no_opinion[0]['rendered']['#markup'] = '<strong><em>' . t('No opinion allowed') . ':</em></strong> ' . t('yes');
        }
        else {
          // Set no opinion text.
          $result->field_field_no_opinion[0]['rendered']['#markup'] = '<strong><em>' . t('No opinion allowed') . ':</em></strong> ' . t('no');
        }
        if (empty($result->field_field_show_bounds[0]['raw']['value'])) {
          // Set no bounds text only if not showing bound.
          $result->field_field_show_bounds[0]['rendered']['#markup'] = '<strong><em>' . t('Show bounds') . ':</em></strong> ' . t('no');
        }
        if (isset($result->field_field_scale_left_value[0]['rendered']['#markup'])) {
          $result->field_field_scale_left_value[0]['rendered']['#markup'] = '<strong><em>' . t('Scale') . ':</em></strong> ' . $result->field_field_scale_left_value[0]['rendered']['#markup'];
        }
        if (!empty($result->field_field_ranking_items[0]['rendered']['#markup'])) {
          // Add label to items to rank.
          $result->field_field_ranking_items[0]['rendered']['#markup'] = '<strong><em>' . t('Items to rank') . ':</em></strong> ' . $result->field_field_ranking_items[0]['rendered']['#markup'];
        }
      }
    }
  }
}
