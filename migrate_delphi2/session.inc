<?php

/**
 * @file
 * Session migration from Delphi 2.
 */

/**
 * Migrates a session from Delphi 2.
 */
class Delphi2SessionMigration extends Migration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $query = Database::getConnection('default', 'delphi2')
      ->select('sessions')
      ->fields('sessions', array(
        'id',
        'created',
        'owner',
        'name',
        'comment',
        'complete',
      ));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('session');
    $source_key = array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ),
    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key, MigrateDestinationNode::getKeySchema(), 'delphi2', array('track_last_imported' => TRUE));

    $this->addFieldMapping('uid', 'owner')
      ->sourceMigration('DelphiUser');
    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('status')
      ->defaultValue(1);
    $this->addFieldMapping('body', 'comment');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('field_session_progress', 'complete');
  }

}
