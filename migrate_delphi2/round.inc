<?php

/**
 * @file
 * Round migration from Delphi 2.
 */

/**
 * Migrates a round from Delphi 2.
 */
class Delphi2RoundMigration extends Migration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $query = Database::getConnection('default', 'delphi2')
      ->select('rounds')
      ->fields('rounds', array(
        'id',
        'created',
        'session_id',
        'number',
        'progress',
      ));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('round');
    $source_key = array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ),
    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key, MigrateDestinationNode::getKeySchema(), 'delphi2', array('track_last_imported' => TRUE));

    $this->addFieldMapping('uid', 'owner')
      ->sourceMigration('DelphiUser');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('status')
      ->defaultValue(1);
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('field_session_progress', 'complete');
    $this->addFieldMapping('field_session', 'session_id')
      ->sourceMigration('DelphiSession');
    $this->addFieldMapping('field_round', 'number');
  }

  /**
   * {@inheritdoc}
   */
  function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $row->title = 'Round ' . $row->number;
    $result = Database::getConnection('default', 'delphi2')
      ->select('sessions')
      ->fields('sessions', array('owner'))
      ->condition('id', $row->session_id)
      ->execute()
      ->fetchObject();
    $row->owner = $result->owner;
    return TRUE;
  }

}
