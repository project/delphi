<?php

/**
 * @file
 * Question migration from Delphi 2.
 */

/**
 * Migrates a question from Delphi 2.
 */
class Delphi2QuestionMigration extends Migration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $query = Database::getConnection('default', 'delphi2')
      ->select('questions', 'q')
      ->fields('q', array(
        'id',
        'ts',
        'round_id',
        'text',
        'type',
        'no_opinion',
        'scale_left_value',
        'scale_left_label',
        'scale_right_value',
        'scale_right_label',
        'units',
        'show_bounds',
      ));
    $query->leftJoin('rounds', 'rounds', 'rounds.id = q.round_id');
    $query->leftJoin('sessions', 'sessions', 'sessions.id = rounds.session_id');
    $query->fields('rounds', array('number', 'session_id'))
      ->fields('sessions', array('owner'));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('question');
    $source_key = array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
        'alias' => 'q',
      ),
    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key, MigrateDestinationNode::getKeySchema(), 'delphi2', array('track_last_imported' => TRUE));

    $this->addFieldMapping('uid', 'owner')
      ->sourceMigration('DelphiUser');
    $this->addFieldMapping('title', 'text_truncated');
    $this->addFieldMapping('body', 'text');
    $this->addFieldMapping('status')
      ->defaultValue(1);
    $this->addFieldMapping('created', 'ts');
    $this->addFieldMapping('field_round', 'number');
    $this->addFieldMapping('field_round_node', 'round_id')
      ->sourceMigration('DelphiRound');
    $this->addFieldMapping('field_session', 'session_id')
      ->sourceMigration('DelphiSession');
    $this->addFieldMapping('field_question_type', 'type');
    $this->addFieldMapping('field_no_opinion', 'no_opinion');
    $this->addFieldMapping('field_scale_left_value', 'scale_left_value');
    $this->addFieldMapping('field_scale_left_label', 'scale_left_label');
    $this->addFieldMapping('field_scale_right_value', 'scale_right_value');
    $this->addFieldMapping('field_scale_right_label', 'scale_right_label');
    $this->addFieldMapping('field_units', 'units');
    $this->addFieldMapping('field_show_bounds', 'show_bounds');
    $this->addFieldMapping('field_ranking_items', 'ranking_item_contents')
      ->separator(',');
    $this->addFieldMapping('field_ranking_items:summary', 'ranking_item_names')
      ->separator(',');
  }

  /**
   * {@inheritdoc}
   */
  function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $query = Database::getConnection('default', 'delphi2')
      ->select('question_ranking_items', 'qri');
    $query->leftJoin('ranked_items', 'ri', 'qri.ranking_item_id = ri.id');
    $query->condition('qri.question_id', $row->id)
      ->fields('ri', array('content', 'name'))
      ->orderBy('ri.id', 'ASC');
    $row->ranking_item_contents = implode(',', $query->execute()->fetchCol(0));
    $row->ranking_item_names = implode(',', $query->execute()->fetchCol(1));
    switch ($row->type) {
      case 'Open-ended':
        $row->type = 'openended';
        break;
      case 'Ranking':
      case 'Scaled':
        $row->type = strtolower($row->type);
        break;
      case 'Text Only':
        $row->type = 'text';
        break;
    }
    $row->text_truncated = substr($row->text, 0, 255);
    $row->text_truncated = preg_replace("/\x80/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x81/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x82/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x83/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x84/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x85/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x86/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x87/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x88/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x89/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x8a/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x8b/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x8c/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x8d/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x8e/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x8f/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x90/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x91/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x92/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x93/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x94/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x95/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x96/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x97/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x98/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x99/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x9a/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x9b/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x9c/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x9d/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x9e/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\x9f/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa0/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa1/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa2/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa3/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa4/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa5/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa6/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa7/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa8/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xa9/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xaa/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xab/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xac/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xad/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xae/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xaf/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb0/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb1/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb2/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb3/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb4/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb5/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb6/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb7/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb8/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xb9/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xba/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xbb/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xbc/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xbd/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xbe/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xbf/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xc0/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xc1/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xc2/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xc3/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xc4/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xc5/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xc6/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xc7/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xc8/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xcb/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xcc/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xe2/", "?", $row->text_truncated);
    $row->text_truncated = preg_replace("/\xe3/", "?", $row->text_truncated);
    return TRUE;
  }

}
