<?php

/**
 * @file
 * User migration from Delphi 2.
 */

/**
 * Migrates a user from Delphi 2.
 */
class Delphi2UserMigration extends Migration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $query = Database::getConnection('default', 'delphi2')
      ->select('admin_users')
      ->fields('admin_users', array(
        'uid',
        'username',
        'password',
        'created',
        'lastname',
        'firstname',
        'email',
      ));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser();
    $source_key = array(
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ),
    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key, MigrateDestinationUser::getKeySchema(), 'delphi2', array('track_last_imported' => TRUE));

    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('is_new')
      ->defaultValue(1);
    $this->addFieldMapping('name', 'username')
      ->dedupe('users', 'name');
    $this->addFieldMapping('status')
      ->defaultValue(1);
    $role = user_role_load_by_name('administrator');
    $this->addFieldMapping('roles')
      ->defaultValue(array(DRUPAL_AUTHENTICATED_RID, $role->rid));
    $this->addFieldMapping('pass', 'password');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('field_lastname', 'lastname');
    $this->addFieldMapping('field_firstname', 'firstname');
    $this->addFieldMapping('mail', 'email');
  }

}

/**
 * Migrates an expert from Delphi 2.
 */
class Delphi2UserExpertMigration extends Migration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $query = Database::getConnection('default', 'delphi2')
      ->select('expert_users')
      ->fields('expert_users', array(
        'uid',
        'created',
        'lastname',
        'firstname',
        'email',
        'active_user_status',
      ));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser();
    $source_key = array(
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ),
    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key, MigrateDestinationUser::getKeySchema(), 'delphi2', array('track_last_imported' => TRUE));

    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('is_new')
      ->defaultValue(1);
    $this->addFieldMapping('name', 'username')
      ->dedupe('users', 'name');
    $this->addFieldMapping('status', 'active_user_status');
    $role = user_role_load_by_name('expert');
    $this->addFieldMapping('roles')
      ->defaultValue(array(DRUPAL_AUTHENTICATED_RID, $role->rid));
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('field_lastname', 'lastname');
    $this->addFieldMapping('field_firstname', 'firstname');
    $this->addFieldMapping('mail', 'email');
  }

  /**
   * {@inheritdoc}
   */
  function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $row->username = $row->firstname . " " . $row->lastname;
    return TRUE;
  }

}
