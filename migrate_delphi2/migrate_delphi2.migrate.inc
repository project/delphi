<?php

/**
 * @file
 * Migration definitions for Delphi 2.
 */

/**
 * Implements hook_migrate_api().
 */
function migrate_delphi2_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'delphi2' => array(
        'title' => t('Delphi 2'),
      ),
    ),
    'migrations' => array(
      'DelphiUser' => array(
        'class_name' => 'Delphi2UserMigration',
        'group_name' => 'delphi2',
      ),
      'DelphiExpert' => array(
        'class_name' => 'Delphi2UserExpertMigration',
        'group_name' => 'delphi2',
      ),
      'DelphiSession' => array(
        'class_name' => 'Delphi2SessionMigration',
        'group_name' => 'delphi2',
      ),
      'DelphiRound' => array(
        'class_name' => 'Delphi2RoundMigration',
        'group_name' => 'delphi2',
      ),
      'DelphiQuestion' => array(
        'class_name' => 'Delphi2QuestionMigration',
        'group_name' => 'delphi2',
      ),
      'DelphiInvite' => array(
        'class_name' => 'Delphi2InviteMigration',
        'group_name' => 'delphi2',
      ),
      'DelphiSubmission' => array(
        'class_name' => 'Delphi2SubmissionMigration',
        'group_name' => 'delphi2',
      ),
    ),
  );
  return $api;
}
