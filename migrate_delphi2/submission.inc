<?php

/**
 * @file
 * Submission migration from Delphi 2.
 *
 * This is not fully developed.
 */

/**
 * Migrates answers from Delphi 2.
 */
class Delphi2SubmissionMigration extends Migration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $query = Database::getConnection('default', 'delphi2')
      ->select('responses')
      ->fields('responses', array(
        'id',
        'ts',
        'question_id',
        'expert_id',
        'answer',
        'comment',
        'confidence_lb',
        'confidence_ub',
      ));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationDelphiWebformSubmission();
    $source_key = array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
      ),
    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key, MigrateDestinationNode::getKeySchema(), 'delphi2', array('track_last_imported' => TRUE));

    $this->addFieldMapping('uid', 'expert_id')
      ->sourceMigration('DelphiExpert');
    $this->addFieldMapping('nid', 'question_id')
      ->sourceMigration('DelphiQuestion');
    $this->addFieldMapping('title', 'text');
    $this->addFieldMapping('created', 'ts');
  }

  /**
   * {@inheritdoc}
   */
  function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    switch ($row->type) {
      case 'Scaled':
        break;
      case 'Ranking':
        break;
      case 'Open-ended':
        break;
      case 'Text Only':
        break;
    }
    return TRUE;
  }

}
