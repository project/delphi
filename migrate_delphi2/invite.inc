<?php

/**
 * @file
 * Invite migration from Delphi 2.
 */

/**
 * Migrates a round experts from Delphi 2.
 */
class Delphi2InviteMigration extends Migration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments) {
    parent::__construct($arguments);
    $query = Database::getConnection('default', 'delphi2')
      ->select('round_experts')
      ->fields('round_experts', array(
        'id',
        'round_id',
        'expert_id',
      ));
    $query->leftJoin('expert_users', 'expert_users', 'expert_users.uid = round_experts.expert_id');
    $query->leftJoin('rounds', 'rounds', 'round_experts.round_id = rounds.id');
    $query->leftJoin('sessions', 'sessions', 'rounds.session_id = sessions.id');
    $query->fields('expert_users', array('email'))
      ->fields('sessions', array('owner'));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationEntityAPI('invite', 'invite_by_email');
    $source_key = array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('Source ID'),
        'alias' => 'round_experts',
      ),
    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key, MigrateDestinationEntityAPI::getKeySchema('invite'), 'delphi2', array('track_last_imported' => TRUE));

    $this->addFieldMapping('uid', 'owner')
      ->sourceMigration('DelphiUser');
    $this->addFieldMapping('inviter', 'owner')
      ->sourceMigration('DelphiUser');
    $this->addFieldMapping('invitee', 'expert_id')
      ->sourceMigration('DelphiUser')
      ->sourceMigration('DelphiExpert');
    $this->addFieldMapping('field_invitation_email_address', 'email');
    $this->addFieldMapping('field_invitation_email_subject')
      ->defaultValue('Delphi Decision Aid session invitation');
    $this->addFieldMapping('field_invitation_email_body')
      ->defaultValue('You have been invited');
    $this->addFieldMapping('field_round_node', 'round_id')
      ->sourceMigration('DelphiRound');
    $this->addFieldMapping('status')
      ->defaultValue(1);
  }

}
