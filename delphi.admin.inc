<?php

/**
 * @file
 * Administration functions for Delphi.
 */

/**
 * Form constructor for Delphi administration.
 */
function delphi_admin_settings_form($form, &$form_state) {
  $form['delphi_expert_invite'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Expert notification email'),
  );
  $form['delphi_expert_invite']['delphi_expert_invite_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('delphi_expert_invite_subject', 'Delphi Decision Aid session invitation'),
  );
  $form['delphi_expert_invite']['delphi_expert_invite_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Message body'),
    '#description' => t('The default message body for the email to notify experts that a round is starting.'),
    '#default_value' => variable_get('delphi_expert_invite_body', 'Dear expert!

You are invited to participate in round [node:field_current_round] of Delphi session [node:title] conducted by [node:author:name].

In order to participate you will need to log in at [invite:invite-accept-link].
To access the Delphi site, please click on the link above or copy and paste it into the address bar of your browser.

You will then be asked to provide input by answering questions designed by [node:author:name].

Your time and input are greatly appreciated. If you have any questions please feel free to contact [site:mail].

You may also login into the Delphi system using this link: [site:login-url]
This link will take you to the session window in which you would be able to view/answer past surveys.'),
  );
  $form['delphi_expert_invite']['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array(
      'user',
      'profile',
      'invite',
      'node',
    ),
  );

  $form['delphi_expert_renotify'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Expert renotify email'),
  );
  $form['delphi_expert_renotify']['delphi_expert_renotify_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('delphi_expert_renotify_subject', 'Delphi Decision Aid session reminder'),
  );
  $form['delphi_expert_renotify']['delphi_expert_renotify_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Message body'),
    '#description' => t('The default message body for the email to remind experts to respond to the questions in a round.'),
    '#default_value' => variable_get('delphi_expert_renotify_body', 'Dear expert!

This email is to remind you about your invitation to participate in Delphi session [node:title] conducted by [node:author:name].

In order to participate you will need to log in at [invite:invite-accept-link]. You will then be asked to provide valuable input in the form of answers to several questions designed by [node:author:name].'),
  );
  $form['delphi_expert_renotify']['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array(
      'user',
      'profile',
      'invite',
      'node',
    ),
  );

  $form['delphi_expert_delete'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Expert deleted notification email'),
  );
  $form['delphi_expert_delete']['delphi_expert_delete_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('delphi_expert_delete_subject', 'Delphi Decision Aid session invitation'),
  );
  $form['delphi_expert_delete']['delphi_expert_delete_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Message body'),
    '#description' => t('The default message body for the email to notify expert an expert that he has been removed from the round.'),
    '#default_value' => variable_get('delphi_expert_delete_body', 'Dear expert!
Your participation in round [node:field_round] of Delphi session [node:field-session:title] conducted by [node:author:name] has been canceled.

If you have any questions please feel free to contact [node:author:name] at [node:author:mail].

You may also login into the Delphi system using this link: [site:login-url]
This link will take you to the session window in which you would be able to view/answer past surveys.'),
  );
  $form['delphi_expert_delete']['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array(
      'user',
      'profile',
      'invite',
      'node',
    ),
  );

  return system_settings_form($form);
}
